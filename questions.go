package main

import "fmt"

func main() {
	//question_1
	question_1()

	//question_2
	x := 3
	for i := 1; i < x+1; i++ {
		fmt.Println(question_2(i))
	}

	//question_3
	var strings_for_question3 []string = []string{"apple", "pie", "apple", "red", "red", "red"}
	question_3(strings_for_question3)
}

func question_1() {
	// Define strings array
	var strings []string = []string{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l", "aabc"}
	var number_of_a []int  //holds number of a's in each word
	var word_lengths []int //holds length of each word
	fmt.Println("Before Sorting")
	fmt.Println(strings)

	//get a's number and word lengths
	k := 0
	for ; k < len(strings); k++ {
		var counter int = 0
		str := strings[k]
		chars := []rune(str)
		for i := 0; i < len(chars); i++ {
			char := string(chars[i])
			if char == "a" {
				counter += 1
			}
			if i+1 == len(chars) {
				word_lengths = append(word_lengths, i+1)
			}
		}
		number_of_a = append(number_of_a, counter)
	}

	//sort only words containing letter a
	for i := 1; i < len(number_of_a); i++ {
		for j := 0; j < i; j++ {
			if number_of_a[j] < number_of_a[i] {
				strings[j], strings[i] = strings[i], strings[j]
				number_of_a[j], number_of_a[i] = number_of_a[i], number_of_a[j]
				word_lengths[j], word_lengths[i] = word_lengths[i], word_lengths[j]
			}
		}
	}

	//sort by same number of characters
	for i := 0; i < len(number_of_a); i++ {
		for j := 0; j < i; j++ {
			if number_of_a[j] == number_of_a[i] && word_lengths[j] < word_lengths[i] {
				strings[j], strings[i] = strings[i], strings[j]
				word_lengths[j], word_lengths[i] = word_lengths[i], word_lengths[j]
			}
		}
	}
	fmt.Println("After Sorting")
	fmt.Println(strings)
}

func question_2(n int) int {
	if n == 1 {
		return 1 + n
	}
	return factorial(n) + n
}

func factorial(x int) int {

	if x == 0 {
		return 1
	}
	var result int = x * factorial(x-1)
	return result
}

func question_3(strings []string) {
	countMap := map[string]int{}
	var maxCnt int
	var freq string
	for _, count := range strings {
		countMap[count]++
		if countMap[count] > maxCnt {
			maxCnt = countMap[count]
			freq = count
		}
	}
	fmt.Println(freq)
}
